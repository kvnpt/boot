from bottle import Bottle, template, static_file, request, response, HTTPError
import interface
import users
from database import bootDb

# Extra feature to follow other users described in 'about' page

application = Bottle()

@application.route('/')
def index():
    """ Handles requests to the homepage (URL /).
    Returns the homepage displaying 50 posts. """

    db = bootDb()

    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    is_admin = None

    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

        # Retrieve all the information of the posts (up to 50).
        list_of_posts = interface.post_list(db)
        pinned_posts = interface.post_list_pinned(db)
        return template('general', title="Productivity Together", content=list_of_posts, pinned=pinned_posts, user=user_loggedin, dbase=db, admin=is_admin)

    return template('welcome', title="Welcome to Productivity Together", content=None, dbase=db, user=None)

@application.route('/users/<usernick:path>')
def userpage(usernick):
    """ Handles requests to a userpage (URL /users/user).
    Returns a page displaying the username, their avatar and their 50 most recent posts. """

    db = bootDb()
    avatar = interface.get_user_avatar(db, usernick)
    # Retrieve all the information of the user's posts.
    user_posts = interface.post_list(db, usernick)


    user_loggedin = None
    is_admin = None
    # user_follow stores whether the logged in user follows the current userpage.
    # 0 = user cannot follow the current userpage
    # 1 = user can follow the current userpage
    # 2 = user is already following the current userpage
    user_follow=0
    # Check if the user is logged in and store their name if so.
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)
        user_follow = 1
        if users.check_following(db, user_loggedin, usernick):
            # User is following this userpage.
            user_follow = 2
        if usernick == user_loggedin:
            # This is the user's own userpage.
            user_follow = 0

    # If the avatar does not exist then the user does not exist so output that the user was not found.
    if avatar is None:
        return template('userpage', title="User not found", usernick=usernick+" not found!", avatar=avatar, dbase=db, content=user_posts, user=user_loggedin, userfollow=user_follow, admin=is_admin)

    return template('userpage', title="Page of " + usernick, usernick=usernick, avatar=avatar, pinned=None, content=user_posts, user=user_loggedin, userfollow=user_follow, admin=is_admin, dbase=db)

@application.route('/mentions/<usernick:path>')
def mentionpage(usernick):
    """ Handles requests to a mentionpage (URL /mentions/user).
    Returns a page displaying a list of the 50 most recent posts mentioning the given user."""

    db = bootDb()
    # Retrieve all the information of posts where the given user is mentioned.
    posts_mentioned = interface.post_list_mentions(db, usernick)

    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    is_admin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

    return template('mentionpage', title="Mentions of "+usernick, content=posts_mentioned, user=user_loggedin, dbase=db, admin=is_admin)

@application.post('/login')
def user_login():
    """ Accepts user POST requests when the user submits the login form.
    If the login is successful it creates a new session cookie and redirects the user to the homepage.
    If the login fails a new page is displayed telling the user to try again."""

    db = bootDb()
    # Check if a username and password has been entered.
    if (request.forms['nick'].strip()) and (request.forms['password']):
        # Then check if the login given is correct.
        if users.check_login(db, request.forms['nick'].strip(), request.forms['password']):
            # Create a new session cookie for the user.
            users.generate_session(db, request.forms['nick'].strip())
            # Return a 303 response and redirect the user to the homepage.
            response.status = 303
            response.set_header('Location', '/')
            return template('login', title="Login success", content="Login success, redirecting now", user=None)

    # In the case the username (and/or) password was wrong or the username (and/or) password was not entered.
    return template('welcome', title="Login Error", content="Login failed. Please try again.", user=None, dbase=db)

@application.route('/logout')
def logout():
    """Logs the user out by deleting their session cookie and returning the homepage."""

    db = bootDb()

    # Delete the user's session cookie.
    users.delete_session(db, users.session_user(db))

    # Redirect the user to the homepage.
    response.status = 303
    response.set_header('Location', '/')
    return "Logout successful"

@application.post('/post')
def post_message():
    """Posts a message for the logged in user."""

    db = bootDb()

    # Get the user currently logged in.
    user = users.session_user(db)

    post_content = (request.forms['post']).strip()

    if (len(post_content) == 0):
        response.status = 303
        response.set_header('Location', '/')
        return "Please type a post"

    try:
        pinned = request.forms['pinned']
    except KeyError:
        pinned = None



    # Add the post entered.
    interface.post_add(db, user, request.forms['post'], pinned)
    # Redirect the user back to the homepage to see a refreshed list of posts.
    response.status = 303
    response.set_header('Location', '/')
    return "Posting"

@application.route('/delete/<post:path>')
def delete_message(post):
    """Deletes a message"""

    db = bootDb()

    interface.post_delete(db, post)

    # Redirect the user back to the homepage to see a refreshed list of posts.
    # Redirect the user back to the homepage to see a refreshed list of posts.
    response.status = 303
    response.set_header('Location', request.headers.get("Referer"))
    return "Deleting"

@application.route('/tag/<tag:path>')
def tags(tag):


    """ Handles requests to a mentionpage (URL /mentions/user).
    Returns a page displaying a list of the 50 most recent posts mentioning the given user."""

    db = bootDb()
    # Retrieve all the information of posts where the given user is mentioned.
    posts = interface.post_list_tags(db, tag)



    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    is_admin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

    return template('mentionpage', title="Mentions of "+tag, content=posts, user=user_loggedin, dbase=db, admin=is_admin)

@application.post('/follow')
def follow():
    """Adds a user as a follower of another user.
    Redirects the user to the user they followed userpage."""

    db = bootDb()

    # Add the user as a follower.
    users.add_follower(db, request.forms['follower'], request.forms['followed'])

    # Redirect the user to the userpage they just followed.
    response.status = 303
    response.set_header('Location', '/users/'+request.forms['followed'])
    return "Setting you as a follower."

@application.post('/unfollow')
def unfollow():
    """Removes a user as a follower of another user.
    Redirects the user to the unfollowed user's userpage."""

    db = bootDb()

    # Add the user as a follower.
    users.delete_follower(db, request.forms['follower'], request.forms['followed'])

    # Redirect the user to the userpage they just followed.
    response.status = 303
    response.set_header('Location', '/users/'+request.forms['followed'])
    return "Taking you off their followers list."

@application.route('/following')
def following():
    """ Handles requests to a following page(URL /following).
    Returns a page displaying a list of the 50 most recent posts by users followed by the logged in user."""

    db = bootDb()
    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    is_admin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

    # Retrieve all the information of posts where the given user is mentioned.
    posts_mentioned = interface.post_list_following(db, user_loggedin)

    return template('following', title="Following", content=posts_mentioned, user=user_loggedin, dbase=db, admin=is_admin)

@application.route('/create')
def create():
    """ Handles requests to a creation page(URL /create).
    Returns a page displaying a form for creating new user."""

    db = bootDb()
    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    is_admin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

    if not users.check_admin(db, user_loggedin):
        response.status = 303
        response.set_header('Location', '/')
        return "Only admins can create accounts"

    # Retrieve all the information of posts where the given user is mentioned.


    return template('create', title="Create Profile", user=user_loggedin, dbase=db, admin=is_admin, content=None)

@application.post('/creation')
def creation():
    """ Accepts user POST requests when the user submits the creation form."""

    db = bootDb()


    user_loggedin = None
    is_admin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)
        is_admin = users.check_admin(db, user_loggedin)

    try:
        admin = request.forms['admin']
    except KeyError:
        admin = None

    # Check if a username and password has been entered.
    if (request.forms['nick'].strip()) and (request.forms['password']):
        if (users.new_user(db, request.forms['nick'].strip(), request.forms['password'], admin)):
            return template('create', title="Creation success", content="Account successfully created", user=user_loggedin, dbase=db, admin=is_admin)
        else:
            return template('create', title="Creation Failed", content="Username already exists", user=user_loggedin, dbase=db, admin=is_admin)

    # In the case the username (and/or) password was not entered.
    return template('create', title="Creation Error", content="Please enter all fields", user=user_loggedin, dbase=db, admin=is_admin)

@application.route('/about')
def about():
    """About page to document any extra features and to make the site a bit more exciting."""
    db = bootDb()

    # Check if the user is logged in and store their name if so.
    user_loggedin = None
    if users.session_user(db):
        user_loggedin = users.session_user(db)

    about_content=""
    return template('about', title="About", content=about_content, user=user_loggedin, dbase=db)


@application.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')

if __name__ == '__main__':
    application.run(debug=True)