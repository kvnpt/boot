% import interface
% rebase('base.tpl')

<header>
    <div id="navigation">
        <a href="/"><img src="/static/bootLogoWhite.png" alt="Productivity Together"></a>
        <a href="/following">Following</a>
        % if admin:
        <a href="/create">Create Profile</a>
        % end
        <a href="/users/{{user}}">Logged in as {{user}}</a>
        <a>
            <form id="logoutform" action="/logout">
                <button class="w3-btn w3-blue" type="submit" value="Logout">Logout</button>
            </form>
        </a>
    </div>
</header>

<div id="newsfeed">
    % for post in content:
        <div class="w3-card-8" id="post">
            <img src="{{post[3]}}">
            <h3><a href='/users/{{post[2]}}'>@{{post[2]}}</a></h3><p>
            {{!interface.post_to_html(post[4])}}<p>
            <h4>{{post[1]}}</h4>
            % if admin or post[2] == user:
            <div id="delete">
                <a href="/delete/{{post[0]}}">Delete</a>
            </div>
            % end
        </div>
    % end
</div>