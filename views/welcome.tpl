% rebase('base.tpl')

<div id="container">
    <div id="login">
        <img src="/static/bootLogo.png" alt="Productivity Together">
        <form class="w3-form" id="loginform" method = "POST" action="/login">
            <input class="w3-input" type="text" name="nick" placeholder="Username">
            <input class="w3-input" type="password" name="password" placeholder="Password">
            <button class="w3-btn w3-blue" type="submit" value="Login">Login</button>
            % if content:
                <p>{{content}}</p>
            % end
        </form>
    </div>
</div>