% rebase('base.tpl')

<article class="Messages">
<h3>New feature(s)</h3>
    <ul>
        <h4>Users can follow/unfollow other users. They can view a list of the posts of the users they have followed.</h4>
        <li>"As a registered user, when I am logged in and visit another user's page
            I should see a 'follow' button which allows me to stay up to date with their latest posts."</li>
        <li> "As a registered user, when I am logged in and visit another user's page which I am
            follower I should see an 'unfollow' button which allows me to stop following that user's posts."</li>
        <li>"As a registered user, when I am logged in I should see a navigation link to a 'following'
            page which displays the latest posts from the users I'm following."</li></ul>
</article>