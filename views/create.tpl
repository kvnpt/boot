% import interface
% rebase('base.tpl')

<header>
    <div id="navigation">
        <a href="/"><img src="/static/bootLogoWhite.png" alt="Productivity Together"></a>
        <a href="/following">Following</a>
        % if admin:
        <a href="/create">Create Profile</a>
        % end
        <a href="/users/{{user}}">Logged in as {{user}}</a>
        <a>
            <form id="logoutform" action="/logout">
                <button class="w3-btn w3-blue" type="submit" value="Logout">Logout</button>
            </form>
        </a>
    </div>
</header>

<div id="newsfeed">
    <form class="w3-form" id="createform" method = "POST" action="/creation">
        <input class="w3-input" type="text" name="nick" placeholder="Username">
        <input class="w3-input" type="password" name="password" placeholder="Password">
        <button class="w3-btn w3-blue" type="submit" value="Create">Create</button>
        <label class="w3-checkbox">
            <input type="checkbox" name="admin">
            <span class="w3-checkmark"></span> Admin
        </label>
    </form>
    % if content:
    <h3>{{content}}</h3>
    % end
</div>

