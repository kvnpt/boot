
<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>{{title}}</title>
        <link rel="shortcut icon" href="/static/favicon.png">
        <link rel="stylesheet" href="/static/style.css">
        <link rel="stylesheet" href="/static/w3.css">
    </head>

    <body>
        {{!base}}
    </body>
</html>
