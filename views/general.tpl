% import interface
% rebase('base.tpl')

<header>
    <div id="navigation">
        <a href="/"><img src="/static/bootLogoWhite.png" alt="Productivity Together"></a>
        <a href="/following">Following</a>
        % if admin:
        <a href="/create">Create Profile</a>
        % end
        <a href="/users/{{user}}">Logged in as {{user}}</a>
        <a>
            <form id="logoutform" action="/logout">
                <button class="w3-btn w3-blue" type="submit" value="Logout">Logout</button>
            </form>
        </a>
    </div>
</header>

<div id="newsfeed">
    <form class="w3-form" id="postform" method="POST" action="/post">
        <textarea class="w3-input" type="text" name="post" placeholder="Write a post here"></textarea>
        <button class="w3-btn w3-blue" type="submit" value="Submit post">Submit Post</button>
        % if admin:
        <label class="w3-checkbox">
            <input type="checkbox" name="pinned">
            <span class="w3-checkmark"></span> Pin this post
        </label>
        % end
    </form>
    % if pinned:
        <h2>Pinned Posts</h2>
        % for post in pinned:
        <div class="w3-card-8 w3-yellow" id="post-pinned">
            <img src="{{post[3]}}">
            <h3><a href='/users/{{post[2]}}'>@{{post[2]}}</a></h3><p>
            {{!interface.post_to_html(post[4])}}<p>
            <h4>{{post[1]}}</h4>
            % if admin or post[2] == user:
            <div id="delete">
                <a href="/delete/{{post[0]}}">Delete</a>
            </div>
            % end
        </div>
        % end
    % end

    <h2>Newsfeed</h2>
    % for post in content:
    <div class="w3-card-8" id="post">
        <img src="{{post[3]}}">
        <h3><a href='/users/{{post[2]}}'>@{{post[2]}}</a></h3><p>
        {{!interface.post_to_html(post[4])}}<p>
        <h4>{{post[1]}}</h4>
        % if admin or post[2] == user:
            <div id="delete">
                <a href="/delete/{{post[0]}}">Delete</a>
            </div>
            % end
    </div>
    % end
</div>