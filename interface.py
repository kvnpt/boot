import re
from itertools import *

def post_to_html(content):
    """Convert a post to safe HTML, quote any HTML code, convert
    URLs to live links and spot any @mentions or #tags and turn
    them into links.  Return the HTML string"""

    # Find all HTML special characters in the string and replace them with entities.
    html = content.replace('&', '&amp;')
    html = html.replace('<', '&lt;')
    html = html.replace('>', '&gt;')

    # Match all URLs within the string.
    find_url = r"(http://(\S+))"
    # Transform all URLs to a HTML link to the resource.
    html = re.sub(find_url, r"<a href='\1'>\1</a>", html)
    # Match all mentions within the string.
    find_mention = r"@((\w|\.\w)+)"
    # Transform all mentions to a HTML link to the user's page.
    html = re.sub(find_mention, r"<a href='/users/\1'>@\1</a>", html)
    # hashtags
    html = re.sub(r"#(\w*)", r"<a href='/tag/\1'>#\1</a>", html)
    return html


def post_list(db, usernick=None, limit=50):
    """Return a list of posts ordered by date
    db is a database connection (as returned by COMP249Db())
    if usernick is not None, return only posts by this user
    return at most limit posts (default 50)

    Returns a list of tuples (id, timestamp, usernick, avatar,  content)
    """
    cursor = db.cursor()

    # In the case a user nickname has been provided
    if usernick:
        # Query to return all the posts and related data associated with the nickname provided
        sql = """SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts
                WHERE users.nick = posts.usernick AND usernick = ?
                ORDER BY timestamp DESC"""
        cursor.execute(sql, (usernick,))
    # Otherwise a nickname has not been provided
    else:
        # Query to return all the posts and related data
        sql = """

                SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts
                WHERE users.nick = posts.usernick
                ORDER BY timestamp DESC"""
        cursor.execute(sql)

    query_result = cursor.fetchall()
    if len(query_result) <= limit:
        return query_result
    # In the case the number of posts exceeds the limit.
    else:
        # Return a list of posts equal to the limit.
        # The most recent <limit-value> posts are returned.
        # e.g. by default the most recent 50 posts are returned.
        limit_result = []
        for x in range(0, limit):
            limit_result.append(query_result[x])
        return limit_result

def post_list_pinned(db):
    cursor = db.cursor()

    sql = """SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts, pinned
                WHERE users.nick = posts.usernick
                AND posts.id = pinned.post
                """
    cursor.execute(sql)
    return cursor.fetchall()

def post_list_mentions(db, usernick, limit=50):
    """Return a list of posts that mention usernick, ordered by date
    db is a database connection (as returned by COMP249Db())
    return at most limit posts (default 50)

    Returns a list of tuples (id, timestamp, usernick, avatar,  content)
    """
    cursor = db.cursor()
    # Query to return all the posts and related data
    sql = """SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts
                WHERE users.nick = posts.usernick
                ORDER BY timestamp DESC"""
    cursor.execute(sql)
    query_result = cursor.fetchall()
    mentioned_posts = []
    # Search through all the posts.
    for element in query_result:
        # element[4] in the element tuple contains the post itself.
        element_content = element[4]
        # Check if the user is mentioned in this post.
        at_value = element_content.find('@' + usernick)
        # If the user is found, add the tuple containing the post onto the mentioned list.
        if at_value >= 0:
            mentioned_posts.append(element)

    if len(mentioned_posts) < limit:
        return mentioned_posts
    # In the case the number of posts exceeds the limit.
    else:
        # Return a list of posts equal to the limit.
        # The most recent <limit-value> posts are returned.
        # e.g. by default the most recent 50 posts are returned.
        limit_result = []
        for x in range(0, limit):
            limit_result.append(mentioned_posts[x])
        return limit_result


def post_add(db, usernick, message, pinned):
    """Add a new post to the database.
    The date of the post will be the current time and date.

    Return a the id of the newly created post or None if there was a problem"""
    cursor = db.cursor()
    # The post is too long, don't add it.
    if len(message) > 150:
        return None

    # Add the post into the database.
    sql = "INSERT INTO posts (usernick, content) VALUES (?, ?)"
    cursor.execute(sql, (usernick, message))

    db.commit()

    # Retrieve the id (auto-generated) from the post just added.
    sql = """SELECT id FROM posts
                WHERE content = ? AND usernick = ?
                ORDER BY timestamp DESC"""
    cursor.execute(sql, (message, usernick))
    query_result = cursor.fetchall()
    # Return the id.
    id = query_result[0][0]

    if pinned:
        sql = "INSERT INTO pinned (post) VALUES (?)"
        cursor.execute(sql, (id,))
        db.commit()

    return id

def post_delete(db, id):
    """Delete a post from the database"""
    cursor = db.cursor()

    # Add the post into the database.
    sql = "DELETE FROM posts WHERE id = ?;"
    cursor.execute(sql, (id,))

    db.commit()

def get_user_avatar(db, usernick):
    """ Retrieve a given user's avatar.
    If no such user hence avatar exists, return None."""
    cursor = db.cursor()

    # Retrieve usernick's avatar from the database.
    sql = "SELECT avatar FROM users WHERE nick=?"
    cursor.execute(sql, (usernick,))
    query_result = cursor.fetchall()

    if query_result:
        # Return the first element of the first tuple of the list which is just the avatar string.
        return query_result[0][0]
    else:
        # If there are no results then the avatar hence the user does not exist.
        return None

def post_list_tags(db, tag, limit=50):
    """Return a list of posts that mention the hashtag, ordered by date
    db is a database connection (as returned by COMP249Db())
    return at most limit posts (default 50)

    Returns a list of tuples (id, timestamp, usernick, avatar,  content)
    """
    cursor = db.cursor()
    sql = "SELECT id, datetime(timestamp, '+11 hours'), usernick, users.avatar, content " \
          "FROM posts INNER JOIN users ON posts.usernick=users.nick"
    table = cursor.execute((sql + " WHERE content LIKE ?"), ('%'+tag+'%', )).fetchall()

    return table[:limit]

def post_list_following(db, usernick, limit=50):
    """Returns a list of posts by those followed by the user."""

    """cursor = db.cursor()
    # Query to return the posts and related data of users followed by the given user.
    sql = ""SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts, follows
                WHERE users.nick = posts.usernick
                AND usernick = ?
                AND follows.follower = posts.usernick
                AND follows.followed <> ?
                ORDER BY timestamp DESC""
    cursor.execute(sql, (usernick, usernick))
    query_result = cursor.fetchall()

    if len(query_result) <= limit:
        return query_result
    # In the case the number of posts exceeds the limit.
    else:
        # Return a list of posts equal to the limit.
        # The most recent <limit-value> posts are returned.
        # e.g. by default the most recent 50 posts are returned.
        limit_result = []
        for x in range(0, limit):
            limit_result.append(query_result[x])
        return limit_result
    """
    cursor = db.cursor()
    # Query to return all the posts and related data
    sql = """SELECT id, datetime(timestamp, '+11 hours'), usernick, avatar, content
                FROM users, posts
                WHERE users.nick = posts.usernick
                ORDER BY timestamp DESC"""
    cursor.execute(sql)
    query_result = cursor.fetchall()

    sql = "SELECT followed FROM follows WHERE follower=?"
    cursor.execute(sql, (usernick,))
    followed_users = cursor.fetchall()
    authored_posts = []
    # Search through all the posts.
    for element in query_result:
        # element[2] in the element tuple is the usernick.
        username = element[2]
        # Check if the user authored this post.
        for list1 in followed_users:
            if username in list1:
                authored_posts.append(element)
        # If the user is found, add the tuple containing the post onto the mentioned list.
    return authored_posts

    """if len(authored_posts) < limit:
        return authored_posts
    # In the case the number of posts exceeds the limit.
    else:
        # Return a list of posts equal to the limit.
        # The most recent <limit-value> posts are returned.
        # e.g. by default the most recent 50 posts are returned.
        limit_result = []
        for x in range(0, limit):
            limit_result.append(authored_posts[x])
        return limit_result"""