import bottle
import interface
import uuid

# this variable MUST be used as the name for the cookie used by this application
COOKIE_NAME = 'sessionid'

def check_login(db, usernick, password):
    """returns True if password matches stored"""
    cursor = db.cursor()

    # Encrypt the password entered by the user.
    password_hash = db.crypt(password)

    # Retrieve the encrypted password from the database.
    sql = "SELECT password FROM users WHERE nick=?"
    cursor.execute(sql, (usernick,))
    query_result = cursor.fetchall()

    # Check if the given password matches the one stored.
    if len(query_result):
        if query_result[0][0] == password_hash:
            return True
    return False

def generate_session(db, usernick):
    """create a new session and add a cookie to the request object (bottle.request)
    user must be a valid user in the database, if not, return None
    There should only be one session per user at any time, if there
    is already a session active, use the existing sessionid in the cookie
    """
    # Check if the user exists, if they do not then return None.
    if interface.get_user_avatar(db, usernick) == None:
        return None

    cursor = db.cursor()

    # Query to find an existing sessionid (if any) for the user.
    sql = "SELECT sessionid FROM sessions WHERE usernick=?"
    cursor.execute(sql, (usernick,))
    query_result = cursor.fetchall()

    if len(query_result):
        # A session id exists so use that in the cookie and return it.
        bottle.response.set_cookie(COOKIE_NAME, query_result[0][0])
        return query_result[0][0]
    else:
        # A session id does not exist for the user.
        # So generate a random sessionid, add it to the database and create a cookie with that id.
        key = str(uuid.uuid4())
        cursor.execute("INSERT INTO sessions VALUES (?, ?)", (key, usernick))
        db.commit()
        bottle.response.set_cookie(COOKIE_NAME, key)
        return key

def delete_session(db, usernick):
    """remove all session table entries for this user"""
    cursor = db.cursor()

    sql = "DELETE FROM sessions WHERE usernick=?"
    cursor.execute(sql, (usernick,))
    db.commit()


def session_user(db):
    """try to
    retrieve the user from the sessions table
    return usernick or None if no valid session is present"""

    # Get the cookie from the user.
    key = bottle.request.get_cookie(COOKIE_NAME)

    # Try to find the username matching the cookie value in the database.
    cur = db.cursor()
    cur.execute("SELECT usernick FROM sessions WHERE sessionid=?", (key,))

    # If the user had the cookie return their usernick otherwise return None.
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        return None

def check_admin(db, user):
    cursor = db.cursor()
    cursor.execute("SELECT usernick FROM admins WHERE usernick=?", (user,))
    query_result = cursor.fetchall()

    # Return True if a result is returned
    if len(query_result):
        if query_result[0][0]:
            return True
    return False

def new_user(db, user, password, admin):
    cursor = db.cursor()

    sql = "SELECT nick FROM users WHERE nick = ?"
    cursor.execute(sql, (user,))

    row = cursor.fetchone()
    if row:
        return False

    sql = "INSERT INTO users (nick, password, avatar) VALUES (?, ?, ?)"
    cursor.execute(sql, (user, db.crypt(password), 'http://robohash.org/'+user))

    if admin:
        sql = "INSERT INTO admins (usernick) VALUES (?)"
        cursor.execute(sql, [user,])

    db.commit()

    return True


def add_follower(db, follower, followed):
    """Add a follower to a user."""
    cursor = db.cursor()
    cursor.execute("INSERT INTO follows VALUES (?, ?)", (follower, followed))
    db.commit()

def check_following(db, user1, user2):
    """Checks if user1 is following user2.
    Returns True if so otherwise False."""
    cursor = db.cursor()

    # Check if an entry exists in the table (i.e. check if user1 is a follower of followed user2)
    sql = "SELECT follower FROM follows WHERE follower=? AND followed=?"
    cursor.execute(sql, (user1, user2))

    query_result = cursor.fetchall()

    # Return True if a result is returned
    if len(query_result):
        if query_result[0][0]:
            return True
    return False

def delete_follower(db, follower, followed):
    """Deletes the follower as a follower of the followed user."""
    cursor = db.cursor()

    sql = "DELETE FROM follows WHERE follower=? AND followed=?"
    cursor.execute(sql, (follower, followed))
    db.commit()
